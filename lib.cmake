#
#
# Include all macros...
# usage:
#     INCLUDE(cmake.lib/lib.env)
#     PARM_ENV_INIT()
#
#
#


GET_FILENAME_COMPONENT(LIB_CURRENT_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)


FILE(GLOB _files ${LIB_CURRENT_DIR}/cmake/*.cmake)
FOREACH(file ${_files})
	INCLUDE(${file})
ENDFOREACH()
