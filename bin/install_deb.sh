#/bin/sh
set -e

_FILE="$1"
if [ "$_FILE" = "" ]; then
	echo  "Flease, specify dependency file" >2
	exit 1
fi


check_version () {
	pkgname="$1"
	minversion="$2"
	
	V="0"
	
	for version in `apt-cache madison $pkgname | awk -F'|' '{print $2}'`; do
	    echo "Considering $version" >@
	    if dpkg --compare-versions $version ge $minversion; then
	        echo "- Version is at least $minversion" >2
	        if dpkg --compare-versions $version gt $V; then
	            echo "- This is the newest version so far" >2
	            V=$version
	        else
	            echo "- We already have a newer version" >2
	        fi
	    else
	        echo "- This is older than $minversion" >2
	    fi
	done
	
	if [ "$V" = "0" ]; then
	    echo "There is no version greater than or equal to $minversion" >2
	    exit 1
	fi

	echo "Selected version: $V" >2
	echo $V
}



while read LINE; do  
	package="`echo "$LINE" | cut -f 1 | tr -d ' '`"
	operator="`echo "$LINE" | cut -f 2 | tr -d '( '`"
	version="`echo "$LINE" | cut -f 3 | tr -d ') '`"


	_sver=""
	if [ "$operator" = ">=" ]; then
		_sver=`check_version "$package" "$version"`
	elif [ "$operator" = "=" ]; then
		_sver="$version"
	fi

	echo apt-get install ${package}=${_sver}
done < "$_FILE"
