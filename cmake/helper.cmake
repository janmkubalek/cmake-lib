CMAKE_MINIMUM_REQUIRED(VERSION 3.0)

IF(DEFINED PAR_HELPER_CMAKE)
	RETURN()
ENDIF()
SET(PAR_HELPER_CMAKE "set")

GET_FILENAME_COMPONENT(PAR_HELPER_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)

INCLUDE(CMakeParseArguments)
INCLUDE(${PAR_HELPER_CMAKE_DIR}/env.cmake)


#
# Same behavior as CMAKE_PARSE_ARGUMENTS, but with "required" addon :)..
# <function>(
#		PREFIX <prefix>
# 		OPTIONS <options>     M
#		MULTI_VALUE <mva>     M
#		ONE_VALUE <ova>       M
#		[REQUIRED <required>] M
#		P_ARGN <p_argn>       M
# 	)
# If we do not specify argument listed in MULTI_VALUE, ONE_VALUE
# the arguments is UNDEFINED. So we can use IF(DEFINED) statement
#
# If we do not specify argument listed in OPTIONS - arguments
# are false!
#
MACRO( PARM_HELPER_PARSE_ARGS )
	SET(options)
	SET(multi_value_args P_ARGN REQUIRED
		OPTIONS MULTI_VALUE ONE_VALUE
	)
	SET(one_value_args PREFIX)
	CMAKE_PARSE_ARGUMENTS(_tmp
		"${options}" "${one_value_args}"
		"${multi_value_args}" ${ARGN}
	)

	CMAKE_PARSE_ARGUMENTS(${_tmp_PREFIX}
		"" "${_tmp_ONE_VALUE};${_tmp_OPTIONS}"
		"${_tmp_MULTI_VALUE}" ${_tmp_P_ARGN}
	)

	FOREACH(req ${_tmp_REQUIRED})
		IF(NOT DEFINED ${_tmp_PREFIX}_${req})
			MESSAGE(FATAL_ERROR "Key '${req}' is not defined!")
		ENDIF()
	ENDFOREACH()

	FOREACH(opt ${_tmp_OPTIONS})
		IF(NOT ${_tmp_PREFIX}_${opt})
			SET(${_tmp_PREFIX}_${opt} FALSE)
		ELSEIF(${_tmp_PREFIX}_${opt} STREQUAL "FALSE" OR
				${_tmp_PREFIX}_${opt} STREQUAL "OFF")
			SET(${_tmp_PREFIX}_${opt} FALSE)
		ENDIF()
	ENDFOREACH()
ENDMACRO()



# 
# Just check, if version is in the valid format ;)
# <function>(
# 		<prefix> <version_array>
# 	)
#
MACRO( PARM_HELPER_PARSE_VERSION _PREFIX _VERSION_LIST )
	LIST(GET ${_VERSION_LIST} 0 ${_PREFIX}_0)
	LIST(GET ${_VERSION_LIST} 1 ${_PREFIX}_1)
	LIST(GET ${_VERSION_LIST} 2 ${_PREFIX}_2)
	IF(${_PREFIX}_0 STREQUAL "")
		MESSAGE(FATAL_ERROR "Please, specify MAJOR version number for ${PARM_CCP_NAME}")
	ENDIF()
	IF(${_PREFIX}_1 STREQUAL "")
		MESSAGE(FATAL_ERROR "Please, specify MINOR version number for ${PARM_CCP_NAME}")
	ENDIF()
	IF(${_PREFIX}_2 STREQUAL "")
		SET(${_PREFIX}_2 0)
	ENDIF()
ENDMACRO()



#
# Convert array to the string :).
# <function>(
# 		ARRAY     <array> M
#		DELIMITER <delimiter>
#		OUTPUT    <output>
# )
#
FUNCTION(PARM_HELPER_JOIN)
	PARM_HELPER_PARSE_ARGS(PREFIX HJ
		ONE_VALUE
			DELIMITER OUTPUT
		MULTI_VALUE
			ARRAY
		REQUIRED
			DELIMITER OUTPUT ARRAY
		P_ARGN ${ARGN}
	)
	STRING(REGEX REPLACE "([^\\]|^);" "\\1${HJ_DELIMITER}" _TMP_STR "${HJ_ARRAY}")
	STRING(REGEX REPLACE "[\\](.)" "\\1" _TMP_STR "${_TMP_STR}") #fixes escaping
	SET(${HJ_OUTPUT} "${_TMP_STR}" PARENT_SCOPE)
ENDFUNCTION()



#
# Copy files from SOURCE_DIR into TARGET_DIR
# <function>(
# 		SOURCE_DIR
#		TARGET_DIR
#		FILE_LIST M
#		TARGET - target to 'anchor' post-build action
# )
#
FUNCTION( PARM_HELPER_COPY_FILES )
	PARM_HELPER_PARSE_ARGS(PREFIX HJ
		ONE_VALUE
			TARGET TARGET_DIR SOURCE_DIR
		MULTI_VALUE
			FILE_LIST
		REQUIRED
			TARGET TARGET_DIR SOURCE_DIR
			FILE_LIST
		P_ARGN ${ARGN}
	)
	FOREACH(filename ${HJ_FILE_LIST})
		SET(source_file ${HJ_SOURCE_DIR}/${filename})
		SET(target_file ${HJ_TARGET_DIR}/${filename})
		IF(IS_DIRECTORY ${source_file})
			ADD_CUSTOM_COMMAND(
				TARGET ${HJ_TARGET}
				POST_BUILD
				COMMAND ${CMAKE_COMMAND} -E copy_directory "${source_file}" "${target_file}"
				VERBATIM
			)
		ELSE()
			ADD_CUSTOM_COMMAND(
				TARGET ${HJ_TARGET}
				POST_BUILD
				COMMAND ${CMAKE_COMMAND} -E copy_if_different "${source_file}" "${target_file}"
				VERBATIM
			)
		ENDIF()
	ENDFOREACH()
ENDFUNCTION()
