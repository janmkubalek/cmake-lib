CMAKE_MINIMUM_REQUIRED(VERSION 3.0)

IF(DEFINED PAR_ARCH_CMAKE)
	RETURN()
ENDIF()
SET(PAR_ARCH_CMAKE "set")

GET_FILENAME_COMPONENT(PAR_ARCH_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)



#
# see on https://github.com/petroules/solar-cmake
# see on files/arch
#
FUNCTION( PARM_ARCH_GET OUTPUT ) 
	ENABLE_LANGUAGE(C)

	TRY_RUN(
		run_result_unused
		compile_result_unused
		"${PAR_ARCH_CMAKE_DIR}"
		"${PAR_ARCH_CMAKE_DIR}/../files/arch.c"
		COMPILE_OUTPUT_VARIABLE arch
	)

	# Parse the architecture name from the compiler output
	string(REGEX MATCH "cmake_ARCH ([a-zA-Z0-9_]+)" arch "${arch}")

	# Get rid of the value marker leaving just the architecture name
	string(REPLACE "cmake_ARCH " "" arch "${arch}")

	# If we are compiling with an unknown architecture this variable should
	# already be set to "unknown" but in the case that it's empty (i.e. due
	# to a typo in the code), then set it to unknown
	if (NOT arch)
		set(arch unknown)
	endif()

	SET(${OUTPUT} ${arch} PARENT_SCOPE)
ENDFUNCTION()

