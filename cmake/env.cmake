cmake_minimum_required(VERSION 3.0)

IF(DEFINED PAR_ENV_CMAKE)
	RETURN()
ENDIF()
SET(PAR_ENV_CMAKE "set")

GET_FILENAME_COMPONENT(PAR_ENV_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)



#
# Set ENV in current scope
#
MACRO( PARM_ENV_INIT)
	IF(EXISTS ${CMAKE_CURRENT_LIST_DIR}/CMakeListsOverride.txt)
		INCLUDE(CMakeListsOverride.txt)
	ENDIF()

	SET(PAR_BUILD_TYPE "Debug"
		CACHE STRING
		"abbrevation for CMAKE_BUILD_TYPE"
	)

	SET(INCLUDE_INSTALL_DIR include/
		CACHE PATH
		"INCLUDE dir for install files"
	)
	SET(LIB_INSTALL_DIR lib/ 
		CACHE PATH
		"LIB dir for install"
	)
	SET(BINARY_INSTALL_DIR bin/ 
		CACHE PATH
		"BIN dir for install"
	)

	SET(PAR_NAMESPACE ""
		CACHE STRING
		"Namespace name (empty string is allowed)"
	)

	
	SET(PAR_INCLUDE_INSTALL_DIR include/${PAR_NAMESPACE}/
		CACHE PATH
		"INCLUDE dir for install par include files"
	)
	SET(PAR_LIB_INSTALL_DIR lib/${PAR_NAMESPACE}/
		CACHE PATH
		"LIB dir for install par lib files"
	)
	SET(PAR_BINARY_INSTALL_DIR bin/${PAR_NAMESPACE}/
		CACHE PATH
		"BIN dir for install par bin files"
	)

	SET(PACKAGE_VENDOR ""
		CACHE STRING
		"Vendor"
	)
	SET(PACKAGE_CONTACT ""
		CACHE STRING
		"Contact"
	)
	
	SET(PACKAGE_ARCHITECTURE "amd64"
		CACHE STRING
		"Package architecture"
	)

	SET(PACKAGE_VERSION_PREFIX ""
		CACHE STRING
		"System package version prefix. Each package have a version in form
		PACKAGE_VERSION_PREFIX<real_package_version>"
	)

	SET(PACKAGE_LIBRARY_PREFIX "lib"
		CACHE STRING
		"Package library prefix"
	)
	SET(PACKAGE_DEVELOPMENT_SUFFIX "-dev"
		CACHE STRING
		"Package suffix for development section"
	)

	__PARM_ENV_CHECK_PLATFORM()
	__PARM_ENV_SET_OUTPUT_DIR()
	__PARM_ENV_SET_PATFORM_SPECS_INSTALL()
ENDMACRO()

#
# RETURN <void>
# Sets the OS_ variables
#
MACRO( __PARM_ENV_CHECK_PLATFORM )
	IF("${CMAKE_SYSTEM_NAME}" STREQUAL "Darwin")
			SET(OS_MACOSX 1)
			SET(OS_POSIX  1)
	ELSEIF("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
			SET(OS_LINUX 1)
			SET(OS_POSIX 1)
	ELSEIF("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
			SET(OS_WINDOWS 1)
	ENDIF()
ENDMACRO()

MACRO( __PARM_ENV_SET_OUTPUT_DIR )
	IF(${CMAKE_GENERATOR} STREQUAL "Ninja")
		# Ninja does not create a subdirectory named after the configuration.
		SET(PAR_TARGET_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
			CACHE PATH
			"Output directory"
		)
	ELSEIF(OS_LINUX)
		SET(PAR_TARGET_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/${PAR_BUILD_TYPE}"
			CACHE PATH
			"Output directory"
		)
	ELSE()
		SET(PAR_TARGET_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/$<CONFIGURATION>"
			CACHE PATH
			"Output directory"
		)
	ENDIF()
ENDMACRO()

MACRO( __PARM_ENV_SET_PATFORM_SPECS_INSTALL )
	IF(OS_LINUX)
		SET(INCLUDE_INSTALL_DIR include/
			CACHE PATH
			"INCLUDE dir for install files"
		)
		SET(LIB_INSTALL_DIR lib/
			CACHE PATH
			"LIB dir for install"
		)
		SET(BINARY_INSTALL_DIR bin/
			CACHE PATH
			"BIN dir for install"
		)
		SET(PAR_INCLUDE_INSTALL_DIR include/${PAR_NAMESPACE}/
			CACHE PATH
			"INCLUDE dir for install par include files"
		)
		SET(PAR_LIB_INSTALL_DIR lib/${PAR_NAMESPACE}/
			CACHE PATH
			"LIB dir for install par lib files"
		)
		SET(PAR_BINARY_INSTALL_DIR bin/${PAR_NAMESPACE}/
			CACHE PATH
			"BIN dir for install par bin files"
		)
	ENDIF()
	IF(OS_WINDOWS OR OS_MACOSX)
			MESSAGE(FATAL_ERROR "Please, set the windows/macos specific install and output directories")
	ENDIF()
ENDMACRO()



#
# RETURN cmake destination...
# <function>(
#	 _varname <varname> // output varuable name
#	 _name <varname>	// name of subdirectory, empty
#							if there is not subdirectory
# )
#
MACRO( PARM_ENV_CMAKE_DESTINATION _varname _name)
	SET(${_varname} ${LIB_INSTALL_DIR}/cmake/${_name}/)
ENDMACRO()

#
# RETURN headers file sdestination...
# <function>(
#	 _varname <varname> // output varuable name
#	 _name <varname>	// name of subdirectory, empty
#							if there is not subdirectory
# )
#
MACRO( PARM_ENV_INCLUDE_DESTINATION _varname)
	SET(${_varname} ${PAR_INCLUDE_INSTALL_DIR}/${ARGV1})
ENDMACRO()

#
# RETURN shared library destination
# <function>(
#	 _varname <varname> // output varuable name
#	 _name <varname>	// name of subdirectory, empty
#							if there is not subdirectory
# )
#
MACRO( PARM_ENV_LIBRARY_DESTINATION _varname)
	PARM_ENV_ARCHIVE_DESTINATION(${_varname} ${ARGV1})
ENDMACRO()

#
# RETURN static library destination
# <function>(
#	 _varname <varname> // output varuable name
#	 _name <varname>	// name of subdirectory, empty
#							if there is not subdirectory
# )
#
MACRO( PARM_ENV_ARCHIVE_DESTINATION _varname)
	SET(${_varname} ${PAR_LIB_INSTALL_DIR}/${ARGV1})
ENDMACRO()

#
# RETURN cpack package vendor :)
# <function>(
#	 _varname <varname> // output varuable name
# )
#
MACRO( PARM_ENV_PACKAGE_VENDOR _varname)
	SET(${_varname} ${PACKAGE_VENDOR})
ENDMACRO()

#
# RETURN cpack package contact
# <function>(
#	 _varname <varname> // output varuable name
# )
#
MACRO( PARM_ENV_PACKAGE_CONTACT _varname)
	SET(${_varname} ${PACKAGE_CONTACT})
ENDMACRO()

#
# RETURN package version prefix
# <function>(
#	 _varname <varname> // output varuable name
# )
#
MACRO( PARM_ENV_PACKAGE_VERSION_PREFIX _varname)
	SET(${_varname} ${PACKAGE_VERSION_PREFIX})
ENDMACRO()

#
# RETURN package library prefix
# <function>(
#	 _varname <varname> // output varuable name
# )
#
MACRO( PARM_ENV_PACKAGE_LIBRARY_PREFIX _varname)
	SET(${_varname} ${PACKAGE_LIBRARY_PREFIX})
ENDMACRO()

#
# RETURN package develepment section suffix
# <function>(
#	 _varname <varname> // output varuable name
# )
#
MACRO( PARM_ENV_PACKAGE_DEVELOPMENT_SUFFIX _varname)
	SET(${_varname} ${PACKAGE_DEVELOPMENT_SUFFIX})
ENDMACRO()
