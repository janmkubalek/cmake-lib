CMAKE_MINIMUM_REQUIRED(VERSION 3.0)

IF(DEFINED PAR_AUTOTOOLS_CMAKE)
	RETURN()
ENDIF()
SET(PAR_AUTOTOOLS_CMAKE "set")

GET_FILENAME_COMPONENT(PAR_AUTOTOOLS_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)

INCLUDE(${PAR_AUTOTOOLS_CMAKE_DIR}/helper.cmake)
INCLUDE(${PAR_AUTOTOOLS_CMAKE_DIR}/dependency.cmake)



#
# <function>(
#	NAME <name>
#	OPT         <configure_opt>   // Option for configure autotools command
#	[DEPENDICES <dependices>]     // Dependices for CONFIGURE_OPT
#								  // in fomat <package_name> \s '(' ('>='|'=') \s <package_version> ')'
#                                 // (debian format)
# )
#
MACRO( PARM_AUTOTOOLS_ADD_CONFIGURE_OPT )
	PARM_HELPER_PARSE_ARGS(PREFIX PARM_AAD
		MULTI_VALUE
			DEPENDICES OPT
		ONE_VALUE
			NAME
		REQUIRED
			NAME OPT
		P_ARGN
			${ARGN}
	)

	LIST(APPEND AUTOTOOLS_${PARM_AAD_NAME}_CONFIGURE_OPT ${PARM_AAD_OPT})

	IF(PARM_AAD_DEPENDICES)
		PARM_DEPENDENCY_ADD(NAME ${PARM_AAD_NAME} DEPENDENCY ${PARM_AAD_DEPENDICES})
	ENDIF()
ENDMACRO() 



#
# <function>(
#	NAME   <name>
#	OUTPUT <output>
#   [BASE_DIR <base_dir>]
# )
#
FUNCTION( PARM_AUTOTOOLS_GENERE_CONFIG_COMMAND)
	PARM_HELPER_PARSE_ARGS(PREFIX PARM_GCC
		ONE_VALUE
			NAME OUTPUT BASE_DIR
		REQUIRED
			NAME OUTPUT
		P_ARGN
			${ARGN}
	)

	SET(conf_opt_list)
	FOREACH(conf_opt ${AUTOTOOLS_${PARM_GCC_NAME}_CONFIGURE_OPT})
		LIST(APPEND conf_opt_list ${conf_opt})
	ENDFOREACH()

	SET(configure_command ./configure)
	IF(${PARM_GCC_BASE_DIR})
		set(configure_command ${PARM_GCC_BASE_DIR})
	ENDIF()

	SET(${PARM_GCC_OUTPUT} ${configure_command} ${conf_opt_list} PARENT_SCOPE)
ENDFUNCTION()



#
# <function>(
#	NAME   <name>
#	OUTPUT <output>
# )
#
MACRO( PARM_AUTOTOOLS_GET_DEPENDICES )
	PARM_HELPER_PARSE_ARGS(PREFIX PARM_GD
		ONE_VALUE
			NAME OUTPUT
		REQUIRED
			NAME OUTPUT
		P_ARGN
			${ARGN}
	)
	PARM_DEPENDENCY_GET(NAME ${PARM_GD_NAME} OUTPUT ${PARM_GD_OUTPUT})
ENDMACRO()



#
# <function>(
#		NAME           <name>
# 		LIBRARY_DIR    <library_dir>
#       INSTALL_PREFIX <install_prefix>
#		[TARGET_PREFIX <target_include_prefix>]
#		TARGETS     <targets>
#		OUTPUT_FILE <output_file>
# )
# Expected directory structure:
#     <library_dir>/lib<target>.so
#     <include_dir>/[<target_include_prefix>]<target>/*.h
#     <include_dir>/*.h
#
FUNCTION( PARM_AUTOTOOLS_CREATE_TARGET_FILE )
	PARM_HELPER_PARSE_ARGS(PREFIX CTF
		ONE_VALUE
			NAME LIBRARY_DIR OUTPUT_FILE
			TARGET_PREFIX INSTALL_PREFIX
			TARGET
		REQUIRED
			TARGET LIBRARY_DIR
			NAME OUTPUT_FILE INSTALL_PREFIX
		P_ARGN
			${ARGN}
	)

	PARM_ENV_INCLUDE_DESTINATION(include_destination)
	PARM_ENV_LIBRARY_DESTINATION(library_destination)

	SET(target_file ${CTF_OUTPUT_FILE})
	SET(library_name ${target})
	SET(target_file_created)

	IF(EXISTS ${CTF_LIBRARY_DIR}/${CTF_TARGET_PREFIX}${target}.so)
		SET(library_name_s ${library_name})
		FILE(APPEND ${target_file}
			"ADD_LIBRARY(${library_name_s} SHARED IMPORTED)
			SET_PROPERTY(TARGET ${library_name_s}
				APPEND PROPERTY
				IMPORTED_LOCATION ${CTF_INSTALL_PREFIX}/${library_destination}/${CTF_TARGET_PREFIX}${target}.so
			)\n
			SET_PROPERTY(TARGET ${library_name_s}
				INTERFACE_INCLUDE_DIRECTORIES ${CTF_INSTALL_PREFIX}/${include_destination}/
			)\n"
		)
		SET(target_file_created TRUE)
	ENDIF()

	IF(EXISTS ${CTF_LIBRARY_DIR}/${CTF_TARGET_PREFIX}${target}.a)
		SET(library_name_a ${library_name}-static)
		FILE(APPEND ${target_file}
			"ADD_LIBRARY(${library_name_a} STATIC IMPORTED)
			SET_PROPERTY(TARGET ${library_name_a}
				APPEND PROPERTY
				IMPORTED_LOCATION ${CTF_INSTALL_PREFIX}/${library_destination}/${CTF_TARGET_PREFIX}${target}.a
			)\n
			SET_PROPERTY(TARGET ${library_name_a}
				INTERFACE_INCLUDE_DIRECTORIES ${CTF_INSTALL_PREFIX}/${include_destination}/
			)\n"
		)
		SET(target_file_created TRUE)
	ENDIF()

	IF(NOT target_file_created)
		MESSAGE(AUTHOR_WARNING "No target file created "
			"(maybe no files for migrate tool? Try build the project and run cmake again)")
	ENDIF()

ENDFUNCTION()




#
# <function>(
#		NAME                 <name>
#		SOURCE_PATH          <include_dir>
#		TARGETS              <targets> M   // this is not CMake target!
#		INSTALL_PREFIX       <install_prefix>
#		[INSTALL_TARGET_FILE  <install_target_file>]
#		[TARGET_PREFIX       <target_prefix>]
#       [OTHER_INCLUDE_FILES <other_files>] M
#       [OTHER_LIB_FILES     <other_lib_files>] M
# )
#
# IF(INSTALL_TARGET_FILE)
#	create the target file
# IF(INSTALL_TARGET_FILE == "APPEND")
#	append export into the exist target file
#
# Header files will be searched in INCLUDE_DIR/<target>/
# for specified target :).
# Expected dir structure:
#     <source_path>/lib/<target>.so
#     <source_path>/lib/<target>.so.*
#     <source_path>/lib/<target>.a
#     <source_path>/lib/<target>.a.*
#     <source_path>/lib/<other_lib_files>
#     <source_path>/include/[<target_prefix>]<target>/*.h
#     <source_path>/include/*.h
#     <source_path>/include/<other_include_files>
#
FUNCTION( PARM_AUTOTOOLS_MIGRATE_LIBRARIES )
	PARM_HELPER_PARSE_ARGS(PREFIX ML
		OPTIONS
			INSTALL_TARGET_FILE
		ONE_VALUE
			TARGET_PREFIX SOURCE_PATH
			NAME INSTALL_PREFIX
		MULTI_VALUE
			TARGETS OTHER_LIB_FILES OTHER_INCLUDE_FILES
		REQUIRED
			SOURCE_PATH
			NAME INSTALL_PREFIX
		P_ARGN
			${ARGN}
	)

	PARM_ENV_INCLUDE_DESTINATION(include_destination)
	PARM_ENV_LIBRARY_DESTINATION(library_destination)

	SET(CPACK_PACKAGING_INSTALL_PREFIX ${ML_INSTALL_PREFIX})

	STRING(TOUPPER ${ML_INSTALL_TARGET_FILE} _append)
	IF(NOT (_append MATCHES "^APPEND$"))
		FILE(REMOVE ${ML_NAME}Target.cmake)
	ENDIF()
	FOREACH(target ${ML_TARGETS})
		IF(ML_INSTALL_TARGET_FILE)
			PARM_AUTOTOOLS_CREATE_TARGET_FILE(NAME ${ML_NAME}
				TARGET      ${ML_TARGETS}
				LIBRARY_DIR ${ML_SOURCE_PATH}/lib
				INSTALL_PREFIX ${ML_INSTALL_PREFIX}
				OUTPUT_FILE ${ML_NAME}Target.cmake
				TARGET_PREFIX ${ML_TARGET_PREFIX}
			)
		ENDIF()

		IF(IS_DIRECTORY ${ML_SOURCE_PATH}/include/${CTF_TARGET_PREFIX}${target})
			INSTALL(DIRECTORY ${ML_SOURCE_PATH}/include/${ML_TARGET_INCLUDE_PREFIX}${target}/
				DESTINATION ${include_destination}
			)
		ENDIF()

		SET(abstract_lib ${ML_SOURCE_PATH}/lib)
		SET(abstract_include ${ML_SOURCE_PATH}/include)

		FILE(GLOB _shared ${abstract_lib}/lib${target}.so*)
		IF(_shared)
			INSTALL(FILES ${_shared}
				DESTINATION ${library_destination}/
			)
		ENDIF()

		FILE(GLOB _static ${abstract_lib}/lib${target}.a*)
		IF(_shared)
			INSTALL(FILES ${_static}
				DESTINATION ${library_destination}/
			)
		ENDIF()

		IF(IS_DIRECTORY ${abstract_include}/${ML_TARGET_PREFIX}${target})
			INSTALL(DIRECTORY ${abstract_include}/${ML_TARGET_PREFIX}${target}
				DESTINATION ${include_destination}/
			)
		ENDIF()
	ENDFOREACH()

	IF(ML_OTHER_LIB_FILES)
		FOREACH(file  ${ML_OTHER_LIB_FILES})
			INSTALL(FILES ${ML_SOURCE_PATH}/lib/${file} DESTINATION ${library_destination})
		ENDFOREACH()
	ENDIF()

	IF(ML_OTHER_INCLUDE_FILES)
		FOREACH(file ${ML_OTHER_INCLUDE_FILES})
			INSTALL(FILES ${ML_SOURCE_PATH}/include/${file} DESTINATION ${library_destination})
		ENDFOREACH()
	ENDIF()

	FILE(GLOB _headers ${ML_SOURCE_PATH}/include/*.h)
	INSTALL(FILES ${_headers}
		DESTINATION ${include_destination}/
	)

	# export target file :)
	IF(ML_INSTALL_TARGET_FILE AND (NOT ML_INSTALL_TARGET_FILE MATCHES "^APPEND$"))
		PARM_ENV_CMAKE_DESTINATION(cmake_destination ${ML_NAME})
		INSTALL(FILES ${ML_NAME}Target.cmake
			DESTINATION ${cmake_destination}
		)
	ENDIF()

ENDFUNCTION()
